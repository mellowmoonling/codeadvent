use alchemical_reduction::{analyze_polymer, reduce_polymer, remove_unit};
use std::fs;

fn main() {
    let input_file = String::from("../input");
    let alphabet_case_pairs: Vec<Vec<char>> = vec![
        vec!['a', 'A'],
        vec!['b', 'B'],
        vec!['c', 'C'],
        vec!['d', 'D'],
        vec!['e', 'E'],
        vec!['f', 'F'],
        vec!['g', 'G'],
        vec!['h', 'H'],
        vec!['i', 'I'],
        vec!['j', 'J'],
        vec!['k', 'K'],
        vec!['l', 'L'],
        vec!['m', 'M'],
        vec!['n', 'N'],
        vec!['o', 'O'],
        vec!['p', 'P'],
        vec!['q', 'Q'],
        vec!['r', 'R'],
        vec!['s', 'S'],
        vec!['t', 'T'],
        vec!['u', 'U'],
        vec!['v', 'V'],
        vec!['w', 'W'],
        vec!['x', 'X'],
        vec!['y', 'Y'],
        vec!['z', 'Z'],
    ];

    let polymer = match fs::read_to_string(input_file) {
        Ok(polymer_str) => polymer_str,
        Err(e) => panic!("Error Reading Polymer from file: {}", e),
    };

    analyze_polymer(&polymer);

    println!("Searching for Shortest polymer");

    let mut shortest_length = polymer.len();
    let mut shortest_polymer_blocker: Vec<char> = Vec::new();

    for pair in &alphabet_case_pairs {
        let reduced = reduce_polymer(&remove_unit(&polymer, pair.clone()));
        let len = reduced.len();

        if len < shortest_length {
            shortest_polymer_blocker = pair.clone();
            shortest_length = len;
        }
    }
    println!(
        "Catalyst for shortest length: {:?} at {}",
        shortest_polymer_blocker, shortest_length
    );
}

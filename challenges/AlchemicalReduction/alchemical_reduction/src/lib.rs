use std::iter::FromIterator;
use std::mem;

pub fn analyze_polymer(polymer: &String) {
    println!("Analyzing polymer string");
    let reduced_polymer = reduce_polymer(polymer);
    println!("Original Units : {}", polymer.len());
    println!("Units Remaining: {}", reduced_polymer.len());
}

pub fn is_opposite(a: char, b: char) -> bool {
    a.eq_ignore_ascii_case(&b) && a != b
}

pub fn reduce_polymer(original_polymer: &String) -> String {
    // convert input string to vec<char>
    let mut root_polymer: Vec<char> = original_polymer.chars().collect();
    let mut reduced_polymer: Vec<char> = Vec::new();
    let mut reduce_flag = true;

    while reduce_flag {
        let mut polymer_iter = root_polymer.iter().peekable();
        while let Some(current) = polymer_iter.next() {
            match polymer_iter.peek() {
                Some(&next) => {
                    if is_opposite(*current, *next) {
                        polymer_iter.next();
                    } else {
                        reduced_polymer.push(*current);
                    }
                }
                None => {
                    // No next value so this is the last one
                    reduced_polymer.push(*current);
                }
            };
        }
        if reduced_polymer.len() != root_polymer.len() {
            mem::swap(&mut root_polymer, &mut reduced_polymer);
            reduced_polymer.clear();
        } else {
            reduce_flag = false;
        }
    }

    return String::from_iter(reduced_polymer);
}

pub fn remove_unit(original_polymer: &String, remove_units: Vec<char>) -> String {
    let root_polymer: Vec<char> = original_polymer.chars().collect();
    let polymer_iter = root_polymer.iter();
    let mut reduced_polymer: Vec<char> = Vec::new();

    for polymer in polymer_iter {
        let mut match_flag = false;
        for unit in &remove_units {
            if *polymer == *unit {
                match_flag = true;
            }
        }
        if !match_flag {
            reduced_polymer.push(*polymer);
        }
    }

    return String::from_iter(reduced_polymer);
}

#[cfg(test)]
use alchemical_reduction::{is_opposite, reduce_polymer, remove_unit};

#[test]
fn capitalize() {
    assert!(is_opposite('a', 'A'));
    assert!(!is_opposite('a', 'b'));
    assert!(!is_opposite('a', 'B'));
    assert!(!is_opposite('a', 'a'));
    assert!(!is_opposite('A', 'A'));
}

#[test]
fn polymer_reduction() {
    let polymer = String::from("dabAcCaCBAcCcaDA");
    println!("Polymer: {}", polymer);
    let reduced = reduce_polymer(&polymer);
    assert_eq!(String::from("dabCBAcaDA"), reduced);
}

#[test]
fn remove_units() {
    let polymer = String::from("dabAcCaCBAcCcaDA");
    assert_eq!(
        String::from("dbcCCBcCcD"),
        remove_unit(&polymer, vec!['a', 'A'])
    );
    assert_eq!(
        String::from("daAcCaCAcCcaDA"),
        remove_unit(&polymer, vec!['b', 'B'])
    );
    assert_eq!(
        String::from("dabAaBAaDA"),
        remove_unit(&polymer, vec!['c', 'C'])
    );
    assert_eq!(
        String::from("abAcCaCBAcCcaA"),
        remove_unit(&polymer, vec!['d', 'D'])
    );
}

#[test]
fn remove_and_reduce() {
    let polymer = String::from("dabAcCaCBAcCcaDA");
    let unit_removed = remove_unit(&polymer, vec!['a', 'A']);
    assert_eq!(String::from("dbCBcD"), reduce_polymer(&unit_removed));
}

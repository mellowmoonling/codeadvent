use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;

struct BoxID {
    id: String,
    dup: bool,
    trp: bool,
}

#[derive(Copy, Clone)]
struct LetterTrack {
    letter: char,
    count: i32,
}

fn build_boxid(boxid: &String) -> BoxID {
    let mut dup = false;
    let mut trp = false;
    let mut record = Vec::<LetterTrack>::new();
    let mut boxid_chars = Vec::new();
    for c in boxid.chars() {
        boxid_chars.push(c);
    }
    let mut unique = boxid_chars.clone();
    unique.dedup();
    for u in &unique {
        let mut cnt = 0;
        for c in &boxid_chars {
            if u == c {
                cnt += 1;
            }
        }
        record.push(LetterTrack {
            letter: *u,
            count: cnt,
        });
    }

    for r in &record {
        if r.count == 2 {
            dup = true;
        } else if r.count == 3 {
            trp = true;
        }
    }

    BoxID {
        id: boxid.clone(),
        dup: dup,
        trp: trp,
    }
}

fn main() {
    let mut box_list = Vec::<BoxID>::new();
    let input_data = Path::new("../input");
    let input_file = File::open(input_data);
    let input_file = match input_file {
        Ok(file) => file,
        Err(error) => {
            panic!("There was a problem opening the file: {:?}", error);
        }
    };
    println!("Inventory Management System");
    println!(" Input File: {}", input_data.display());
    // Input is box ids
    // For each id
    //  - Determine if it has any letters that occur exactly twice
    //  - Determine if it has any letters that occur exactly three times
    // Checksum : (number of ids with double letters) *
    //            (number of ids with triple letters)

    let buffered = BufReader::new(input_file);
    for line in buffered.lines() {
        match line {
            Ok(line_contents) => {
                // String
                box_list.push(build_boxid(&line_contents));
            }
            Err(error) => {
                panic!("There was a problem reading line {:?}", error);
            }
        }
    }
    let mut num_dups = 0;
    let mut num_trps = 0;
    for b in &box_list {
        if b.dup {
            num_dups += 1;
        }
        if b.trp {
            num_trps += 1;
        }
    }
    println!("Checksum: {}", (num_dups * num_trps));

    for master_boxid in &box_list {
        for compare_boxid in &box_list {
            let mut strikes = 0;
            if master_boxid.id != compare_boxid.id {
                for (mc, cc) in master_boxid.id.chars().zip(compare_boxid.id.chars()) {
                    if mc != cc {
                        strikes += 1
                    }
                    if strikes >= 2 {
                        break;
                    }
                }
            }
            if strikes == 1 {
                println!{"MATCH:"};
                println!{" {}", master_boxid.id};
                println!{" {}", compare_boxid.id};
                // TODO: Print only matching
            }
        }
    }
}

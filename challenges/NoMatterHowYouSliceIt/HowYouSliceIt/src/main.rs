#![allow(dead_code)]
#![allow(non_snake_case)]
use std::collections::HashMap;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(PartialEq, Copy, Clone)]
struct Coord {
    x: i32,
    y: i32,
}

struct SheetSize {
    height: i32,
    width: i32,
}

type Fabric = HashMap<(i32, i32), i32>;

#[derive(PartialEq, Copy, Clone)]
struct Claim {
    id: i32,
    width: i32,
    height: i32,
    overlap: bool,
    pos_ul: Coord,
    pos_ur: Coord,
    pos_ll: Coord,
    pos_lr: Coord,
}

impl Claim {
    fn file_claim(
        id: i32,
        left_dist: i32,
        top_dist: i32,
        width: i32,
        height: i32,
        sheet: &SheetSize,
    ) -> Claim {
        // Assume origin is lower left (0,0) of total sheet
        Claim {
            id: id,
            width: width,
            height: height,
            overlap: false,
            pos_ul: Coord {
                x: left_dist,
                y: (sheet.height - top_dist),
            },
            pos_ur: Coord {
                x: (left_dist + width),
                y: (sheet.height - top_dist),
            },
            pos_ll: Coord {
                x: left_dist,
                y: (sheet.height - top_dist - height),
            },
            pos_lr: Coord {
                x: (left_dist + width),
                y: (sheet.height - top_dist - height),
            },
        }
    }
}

fn read_claims(claims: &mut std::vec::Vec<Claim>, sheet: &SheetSize) {
    let input_file = match File::open("../input") {
        Ok(file) => file,
        Err(error) => panic!("Error Opening File {:?}", error),
    };

    let buffered = BufReader::new(input_file);
    for line in buffered.lines() {
        // parsing logic
        let mut id_num = 0;
        let mut left_dist = 0;
        let mut top_dist = 0;
        let mut height = 0;
        let mut width = 0;
        let line_string = match line {
            Ok(l) => l,
            Err(e) => panic!("Cannot Parse Line: {:?}", e),
        };
        let line_tokens = line_string.split(' ').collect::<Vec<&str>>();
        for (i, token) in line_tokens.iter().enumerate() {
            match i {
                0 => id_num = token[1..].parse::<i32>().unwrap(),
                1 => continue,
                2 => {
                    let position_tokens = token.split(',').collect::<Vec<&str>>();
                    left_dist = position_tokens[0].parse::<i32>().unwrap();
                    let top_tokens = position_tokens[1].split(':').collect::<Vec<&str>>();
                    top_dist = top_tokens[0].parse::<i32>().unwrap();
                }
                3 => {
                    let measure_tokens = token.split('x').collect::<Vec<&str>>();
                    width = measure_tokens[0].parse::<i32>().unwrap();
                    height = measure_tokens[1].parse::<i32>().unwrap();
                }
                _ => panic!("Extra Field while parsing: {}", line_string),
            };
        }
        claims.push(Claim::file_claim(
            id_num, left_dist, top_dist, width, height, &sheet,
        ));
    }
}

fn record_claims(claims: &std::vec::Vec<Claim>, fabric: &mut Fabric) {
    println!("Processing {} claims", claims.len());

    for claim in claims {
        for x in claim.pos_ul.x..claim.pos_ur.x {
            for y in claim.pos_ll.y..claim.pos_ur.y {
                let element = fabric.entry((x, y)).or_insert(0);
                *element += 1;
            }
        }
    }
}

fn read_claims_debug(claims: &mut std::vec::Vec<Claim>, sheet_size: &SheetSize) {
    claims.push(Claim::file_claim(1, 1, 3, 4, 4, &sheet_size));
    claims.push(Claim::file_claim(2, 3, 1, 4, 4, &sheet_size));
    claims.push(Claim::file_claim(3, 5, 5, 2, 2, &sheet_size));
}

fn main() {
    println!("How Do You Slice It?");
    let mut total_overlap = 0;
    let mut claims: std::vec::Vec<Claim> = Vec::new();
    let mut fabric = Fabric::new();
    let sheetsize = SheetSize {
        height: 1000,
        width: 1000,
    };

    read_claims(&mut claims, &sheetsize);
    //read_claims_debug(&mut claims, &sheetsize);
    record_claims(&claims, &mut fabric);

    for v in fabric.values() {
        if *v >= 2 {
            total_overlap += 1;
        }
    }

    println! {"Total Overlap: {} in^2", total_overlap};
    let mut non_overlapped: std::vec::Vec<Claim> = Vec::new();

    for claim in &claims {
        let mut non_overlap_flag = false;
        let mut num_non_overlapping = 0;
        let num_elements = (claim.pos_ur.x - claim.pos_ul.x) * (claim.pos_ur.y - claim.pos_lr.y);
        for x in claim.pos_ul.x..claim.pos_ur.x {
            for y in claim.pos_ll.y..claim.pos_ul.y {
                let element_count = fabric.get(&(x, y)).unwrap();
                if *element_count == 1 {
                    num_non_overlapping += 1;
                }
            }
        }
        if num_non_overlapping == num_elements {
            non_overlapped.push(claim.clone());
        }
    }

    println!("Non Overlapped Claims: {}", non_overlapped.len());
    // for (i, c) in non_overlapped.iter().enumerate() {
    //     println!("[{}] {}", i, c.id);
    // }
    if non_overlapped.len() == 1 {
        println!("Single Non Overlapping Claim ID: {}", non_overlapped[0].id);
    }
}

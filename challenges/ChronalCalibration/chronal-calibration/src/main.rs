use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;

fn main() {
    let mut calibration = 0;
    let mut readings = Vec::new();
    let mut cal_history = Vec::new();
    let input_data = Path::new("../input");

    println!("Chronal Calibration!");
    println!(" Input File: {}", input_data.display());

    let input_file = File::open(input_data);
    let input_file = match input_file {
        Ok(file) => file,
        Err(error) => {
            panic!("There was a problem opening the file: {:?}", error);
        }
    };
    // Get Readings
    let buffered = BufReader::new(input_file);
    for line in buffered.lines() {
        match line {
            Ok(mut line_contents) => match line_contents.parse::<i32>() {
                Ok(value) => {
                    readings.push(value);
                }
                Err(error) => {
                    panic!{"Unknown value while parsing: {:?}",error};
                }
            },
            Err(error) => {
                panic!("There was a problem reading line {:?}", error);
            }
        }
    }
    // Initial Reading
    for reading in &readings {
        calibration += reading;
        cal_history.push(calibration);
    }

    println!("Part One Final Calibration: {}", calibration);
    println!("Searching For First Duplicate Calibration Reading");
    println!("Clearing calibration history");
    let mut found_duplicate = false;
    let mut loop_cnt = 0;
    let loop_lim = 1000;
    calibration = 0;
    cal_history.clear();

    while !found_duplicate {
        for reading in &readings {
            calibration += reading;
            if cal_history.contains(&calibration) {
                println!("Found Duplicate: {}", calibration);
                println!("After {} Entries", cal_history.len());
                found_duplicate = true;
                break;
            }
            cal_history.push(calibration);
        }
        if loop_cnt >= loop_lim {
            panic!("Too many reading iterations");
        } else {
            loop_cnt += 1;
        }
    }
}

use chrono::Duration;
use chrono::{NaiveDate, NaiveDateTime, NaiveTime, Timelike};
use std::collections::HashMap;
use std::io::BufRead;
use ResponseRecord;

struct MinRecord {
    by_min: HashMap<NaiveTime, i32>,
    total: chrono::Duration,
}

fn build_record(events: &Vec<ResponseRecord::Event>) -> HashMap<i32, MinRecord> {
    let mut gaurds_sleep_count: HashMap<i32, MinRecord> = HashMap::new();
    let mut current_gaurd_id = 0;
    let mut sleep_start = NaiveDateTime::new(
        NaiveDate::from_ymd(1518, 10, 31),
        NaiveTime::from_hms(0, 0, 0),
    );
    println!("Iterating Over Events");
    for e in events {
        match e.action {
            ResponseRecord::Action::ChangingTo(id) => current_gaurd_id = id,
            ResponseRecord::Action::FallingAsleep => sleep_start = e.time,
            ResponseRecord::Action::Waking => {
                let sleep_count = gaurds_sleep_count
                    .entry(current_gaurd_id)
                    .or_insert(MinRecord {
                        by_min: HashMap::new(),
                        total: chrono::Duration::zero(),
                    });
                sleep_count.total = sleep_count.total + (e.time - sleep_start);
                let mut m = sleep_start.time();
                while m < e.time.time() {
                    let minute_count = sleep_count.by_min.entry(m).or_insert(0);
                    *minute_count += 1;
                    m += Duration::minutes(1);
                }
            }
        }
    }

    return gaurds_sleep_count;
}

fn build_sleep_frequency_record(record: &HashMap<i32, MinRecord>) {
    let mut count_max = 0;
    let mut gaurd_id = 0;
    let mut time_stamp = NaiveTime::from_hms(0, 0, 0);

    // for each gaurd find the minute that they are asleep most

    for (&id, min_rec) in record.iter() {
        for (min, &count) in min_rec.by_min.iter() {
            if count > count_max {
                count_max = count;
                gaurd_id = id;
                time_stamp = *min
            }
        }
    }

    println!("Part Two __________");
    println!("Gaurd {}", gaurd_id);
    println!("Count {}", count_max);
    println!("Time  {}", time_stamp);
    println!("Answer: {}", (gaurd_id * time_stamp.minute() as i32));
}

fn find_sleepiest_gaurd(record: &HashMap<i32, MinRecord>) -> i32 {
    let mut max_min = 0;
    let mut gaurd_id = 0;

    for (&id, min_rec) in record.iter() {
        if min_rec.total.num_minutes() > max_min {
            max_min = min_rec.total.num_minutes();
            gaurd_id = id;
        }
    }

    return gaurd_id;
}

fn get_sleepy_min(min_record: &MinRecord) -> NaiveTime {
    let mut max_dur = 0;
    let mut min_entry = NaiveTime::from_hms(0, 0, 0);

    for (&min, &dur) in &min_record.by_min {
        if dur > max_dur {
            max_dur = dur;
            min_entry = min;
        }
    }

    return min_entry;
}

fn main() {
    println!("Response Record Challenge");

    //let input_file = String::from("../test-input");
    let input_file = String::from("../input");

    let input_lines = ResponseRecord::read_input_file(&input_file);
    let mut events: Vec<ResponseRecord::Event> = Vec::new();

    for line in input_lines.lines() {
        match line {
            Ok(l) => events.push(ResponseRecord::parse_line(&l)),
            Err(e) => println!("Error: {:?}", e),
        }
    }

    events.sort_by(|a, b| a.time.cmp(&b.time));
    let gaurd_sleep_record = build_record(&events);
    let sleepy_gaurd = find_sleepiest_gaurd(&gaurd_sleep_record);

    let gaurd_rec = gaurd_sleep_record.get(&sleepy_gaurd).unwrap();
    let sleepy_min = get_sleepy_min(&gaurd_rec);

    println!("Gaurd ID: {}", sleepy_gaurd,);
    println!("Sleepiest Min: {}", sleepy_min);
    println!("Part One: {}", (sleepy_gaurd * sleepy_min.minute() as i32));

    let minute_sleep_record = build_sleep_frequency_record(&gaurd_sleep_record);

    // println!("Minute that is slept the most: {}", min_entry);
    // println!("Duration: {}", max_dur);
    // println!("Answer: {}", (max_dur * gaurd_id));
}

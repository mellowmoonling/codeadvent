extern crate chrono;
extern crate regex;
use chrono::NaiveDateTime;
use regex::Regex;
use std::fs::File;
use std::io::BufReader;

#[derive(Debug)]
pub enum Action {
    Waking,
    FallingAsleep,
    ChangingTo(i32),
}

#[derive(Debug)]
pub struct Event {
    pub time: NaiveDateTime,
    pub action: Action,
}

pub fn read_input_file(file_str: &String) -> BufReader<std::fs::File> {
    let input_file = match File::open(file_str) {
        Ok(file) => file,
        Err(error) => panic!("Error Opening File {:?}", error),
    };

    let buffered = BufReader::new(input_file);
    return buffered;
}

pub fn parse_line(line: &String) -> Event {
    let first_pass =
        match Regex::new(r"\[(?P<timestamp>\d{4}-\d{2}-\d{2} \d{2}:\d{2})\] (?P<rest>.*)$") {
            Ok(r) => r,
            Err(e) => panic!("Error Compiling Regex: {:?}", e),
        };
    let first_caps = match first_pass.captures(line) {
        Some(cap) => cap,
        None => panic!("No Captures Found"),
    };
    let timestamp_str = match first_caps.name("timestamp") {
        Some(ts) => ts.as_str(),
        None => panic!("Error parsing timestamp string"),
    };
    let timestamp = match NaiveDateTime::parse_from_str(timestamp_str, "%Y-%m-%d %H:%M") {
        Ok(ts) => ts,
        Err(e) => panic!("Parsing Error: {:?} while parsing {:?}", e, timestamp_str),
    };

    let action_pass = match Regex::new(
        r"(?P<wake>wakes up)|(?P<sleep>falls asleep)|(?P<change>Guard #(?P<id>\d+) begins shift)",
    ) {
        Ok(r) => r,
        Err(e) => panic!("Error with action pass {:?}", e),
    };
    //    let rest_str = first_caps.name("rest").unwrap().as_str();
    let rest_str = match first_caps.name("rest") {
        Some(cap) => cap.as_str(),
        None => panic!("Error with action pass capture"),
    };
    let action_caps = match action_pass.captures(rest_str) {
        Some(cap) => cap,
        None => panic!("Error with action caps"),
    };
    let action = match action_caps.get(0) {
        Some(m) => match m.as_str() {
            "wakes up" => Action::Waking,
            "falls asleep" => Action::FallingAsleep,
            _ => match action_caps.name("change") {
                Some(_) => {
                    let id = action_caps
                        .name("id")
                        .unwrap()
                        .as_str()
                        .parse::<i32>()
                        .unwrap();
                    Action::ChangingTo(id)
                }
                _ => panic!("Error parsing {:?}", action_caps),
            },
        },
        _ => panic!("Error Parsing Action: {:?}", action_caps.get(0)),
    };

    Event {
        time: timestamp,
        action: action,
    }
}
